//============================================================================
// Name        : Aula20180308.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Aula20180308.h"
using namespace std;


int inline Retangulo::area(){ //forcando inline
	return largura * altura;
}

int Retangulo::perimetro(){
	return 2 * (largura + altura);
}

void Retangulo::setAltura(int altura){
	this->altura = altura;
}


int main() {
	Retangulo ret1(10, 5);
	Retangulo ret2;

	Retangulo *r = new Retangulo(10, 15);
	Retangulo *array = new Retangulo[10];

	cout << ret1.area() << endl;
	cout << ret2.area() << endl;

	int a = 3;
	int b = 6;
	//cin >> a >> b;
	cout << "antes --> a: " << a << " - b: " << b <<endl;
	swap(a, b);//troca o valor de duas variaveis
	cout << "depois --> a: " << a << " - b: " << b <<endl;

	delete r;
	delete[] array;

	return 0;
}

