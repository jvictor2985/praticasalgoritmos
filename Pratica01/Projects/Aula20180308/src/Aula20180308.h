/*
 * Aula20180308.h
 *
 *  Created on: 8 de mar de 2018
 *      Author: ALUNO
 */

#ifndef AULA20180308_H_
#define AULA20180308_H_

class Retangulo{
private:
	int largura;
	int altura;
public:
	Retangulo(){};
	Retangulo(int a, int l){
		this->altura = a;
		this->largura = l;
	};
	int area();
	int perimetro();
	//~Retangulo(){};//destrutor padrao
	~Retangulo(){
		//cout <<" Destruido2" << endl;
	};

	int getAltura(){return this->altura;};
	void setAltura(int altura);
};



#endif /* AULA20180308_H_ */
