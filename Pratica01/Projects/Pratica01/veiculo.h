#ifndef VEICULO_H_INCLUDED
#define VEICULO_H_INCLUDED
using namespace std;

class Roda{
public:
    Roda(){
        cout << "Objeto Roda " << " foi construido." << endl;
    }
    ~Roda(){
        cout << "Objeto Roda" << " foi destruido." << endl;
    }
};

class Veiculo{
private:
	string nome;
	int num_rodas;
	Roda *rodas;
public:
	Veiculo(const char* nome){
	    rodas = NULL;
		this->nome = string(nome);
		this->num_rodas = 0;
		cout << "Objeto " << this->nome << " foi construido." << endl;
	};
	~Veiculo(){
		cout << "Objeto " << this->nome << " foi destruido." << endl;
		delete [] rodas;
	};
	int getNumRodas();
	void setNumRodas(int numRodas);
};

#include "veiculo.cpp"
#endif // VEICULO_H_INCLUDED
