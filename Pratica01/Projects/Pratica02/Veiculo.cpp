int Terrestre::getCapacidadeMax(){
    return this->cap_pass;
}
void Terrestre::setCapacidadeMax(int newCapPass){
    this->cap_pass = newCapPass;
}
float Aquatico::getCargaMax(){
    return this->carga_max;
}
void Aquatico::setCargaMax(int newCargaMax){
    this->carga_max = newCargaMax;
}

float Aereo::getVelocidadeMax(){
    return this->vel_max;
}
void Aereo::setVelocidadeMax(int newVelMax){
    this->vel_max = newVelMax;
}

/*void Veiculo::mover(){
    cout << "Veiculo " << this->nome << " moveu." << endl;
}*/
void Terrestre::mover(){
    cout << "Veiculo Terrestre " << this->nome << " moveu." << endl;
}
void Aquatico::mover(){
    cout << "Veiculo Aquatico " << this->nome << " moveu." << endl;
}
void Aereo::mover(){
    cout << "Veiculo Aereo " << this->nome << " moveu." << endl;
}
void Anfibio::mover(){
    Terrestre::mover();
    Aquatico::mover();
}
