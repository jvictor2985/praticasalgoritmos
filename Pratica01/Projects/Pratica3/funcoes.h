#ifndef FUNCOES_H_INCLUDED
#define FUNCOES_H_INCLUDED

#include <cstring>
template <class T>
void trocar(T & a, T & b) {
    T c;
    c=a;
    a=b;
    b=c;
}
template <class T>
T maximo(const T a, const T b) {
    if(a > b)
        return a;
    else
        return b;
}
template <class T>
T minimo(const T a, const T b) {
    if(a < b)
        return a;
    else
        return b;
}
template <>
char* maximo<char*>(char* a, char* b) {
    if(strcmp(a, b) > 0)
        return a;
    else
        return b;
}

#endif // FUNCOES_H_INCLUDED
