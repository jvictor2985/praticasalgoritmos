#ifndef FILA_H_INCLUDED
#define FILA_H_INCLUDED

using namespace std;

template <class T>
class Fila {
private:
// array de itens, capacidade, tamanho, posi��o inicial, etc.
T *itens;
int capacity;
int queueSize;
int startId;
int endId;

public:
Fila(int cap) {
// inicializar array de items, capacidade, tamanho, posi��o inicial
    this->capacity = cap;
    this->queueSize = 0;
    this->endId = 0;
    this->startId = 0;
    this->itens = new T[this->capacity];
}
~Fila() {
// destruir array de itens
    delete[] this->itens;
}
void enfileira(const T &item) {
// adiciona um item ao final da fila; lan�a �Fila cheia� caso cheia
    if(this->queueSize < capacity){
        //cout << "Adcionando " << item << " no ID: " << (this->endId) % this->capacity << endl;
        this->itens[(this->endId)] = item;
        this->endId = (this->endId + 1) % this->capacity;
        this->queueSize++;
        //cout << "Pos0: " << this->itens[this->startId] << endl;
    }else{
        cout << "Fila cheia" << endl;
    }
}
T desenfileira() {
// remove um item do inicio da fila; lan�a �Fila vazia� caso vazia
    T aux;
    if(this->queueSize > 0){
        //cout << "Tirando: " << this->itens[this->startId] << " do ID: " << startId <<endl;
        aux = this->itens[this->startId];
        this->startId = (this->startId + 1) %this->capacity;
        this->queueSize--;
        return aux;
    }else{
        cout << "Fila vazia" << endl;
    }
}
int cheia() {
// retorna 1 se cheia, 0 caso contr�rio
    if(this->queueSize == this->capacity){
        return 1;
    }else{
        return 0;
    }
}
int vazia() {
// retorna 1 se vazia, 0 caso contr�rio
    if(this->queueSize == 0){
        return 1;
    }else{
        return 0;
    }
}
int tamanho() {
//retorna a quantidade de itens atualmente na fila
    return this->queueSize;
}
};


#endif // FILA_H_INCLUDED
