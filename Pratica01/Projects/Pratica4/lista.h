#ifndef LISTA_H_INCLUDED
#define LISTA_H_INCLUDED

using namespace std;

template <class T>
class Lista {
private:
// itens da lista, capacidade e tamanho atual
T *itens;
int capacity;
int size;
public:
Lista(int capacidade) {
// inicilizacao do array, capacidade e tamanho
    this->capacity = capacidade + 1;
    this->itens = new T[this->capacity];
    this->size = 0;//alterar para 1?
}
~Lista() {
//destruicao do array
    delete this->itens;
}
void adiciona (const T & item) {
// adiciona um item ao final da lista; lanca "Lista cheia" caso cheia
    if(this->size - 1 < this->capacity){
        this->itens[size] = item;
        size++;
    }else{
        cout << "A Lista cheia" << endl;
    }
}
T pega(int idx) {
// pega um item pelo indice (comeca em 1);
// lanca "Item invalido" se posicao invalida
    if(idx < 1 || idx - 1 > this->size){
        cout << "P Item invalido" << endl;
    }else{
        return this->itens[idx - 1];
    }
}
void insere (int idx, const T & item) {
// insere um item na posicao indicada
// lanca "Lista cheia" caso cheia
// lanca "Item invalido" se posicao invalida
// desloca itens existentes para a direita
    if(this->size >= this->capacity - 1){
        cout << "Lista cheia" << endl;
    }else if(idx < 1 || idx > this->size){
        cout << "I Item invalido" << endl;
    }else{
        for(int i = size; i >= (idx - 1); i--){
            this->itens[i] = this->itens[i - 1];
        }
        this->itens[idx - 1] = item;
        size++;
    }
}
void remove(int idx) {
// remove item de uma posicao indicada
// lanca "Item invalido" se posicao invalida
// desloca itens para a esquerda sobre o item removido
    if(idx < 1 || idx > size){
        cout << "R Item invalido" << endl;
    }else{
        for(int i = idx - 1; i < size; i++){
            this->itens[i] = this->itens[i + 1];
        }
        size--;
    }
}
void exibe() {
// exibe os itens da saida padrao separados por espacos
    for(int i = 0; i < this->size; i++){
        cout << this->itens[i] << " ";
    }
    cout << endl;
}
};


#endif // LISTA_H_INCLUDED
