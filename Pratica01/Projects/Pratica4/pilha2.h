#ifndef PILHA2_H_INCLUDED
#define PILHA2_H_INCLUDED

template <class T>
struct nodePilha{
    nodePilha *next;
    T item;
};

template <class T>
class Pilha {
private:
// propriedades para array de items, capacidade e topo da pilha
    nodePilha<T> *top;
public:
Pilha() {
// instancia array de items, inicializa capacidade e topo
}
~Pilha() {
// destroy array de items
}
void empilha(T item) {
// empilha um item no topo da pilha; lan�a �Estouro da pilha� se cheia
    nodePilha<T> *newNodePilha = new nodePilha<T>;
    newNodePilha->next = top;
    newNodePilha->item = item;
    top = newNodePilha;
}
T desempilha() {
// remove um item do topo da pilha; lan�a �Pilha vazia� se vazia
    T returnedItem = top->item;
    nodePilha<T> *auxNodePilha = top;
    top = top->next;
    delete auxNodePilha;
    return returnedItem;
}
};

#endif // PILHA2_H_INCLUDED
