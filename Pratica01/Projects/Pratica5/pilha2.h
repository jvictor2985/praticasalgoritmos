#ifndef PILHA2_H_INCLUDED
#define PILHA2_H_INCLUDED

#include <iostream>
using namespace std;
template <class T>
struct nodePilha{
    nodePilha *next;
    T item;
};

template <class T>
class Pilha {
private:
// propriedades para array de items, capacidade e topo da pilha
    nodePilha <T>*top;
    int tam;
public:
Pilha() {
// instancia array de items, inicializa capacidade e topo
}
~Pilha() {
// destroy array de items
    for(int i = 0; i < tam; i++){
        desempilha();
    }
}
void empilha(T item) {
// empilha um item no topo da pilha; lança “Estouro da pilha” se cheia
    nodePilha<T> *newNodePilha = new nodePilha<T>;
    newNodePilha->next = top;
    newNodePilha->item = item;
    top = newNodePilha;
    tam++;
}
T desempilha() {
// remove um item do topo da pilha; lança “Pilha vazia” se vazia
    T returnedItem = top->item;
    nodePilha<T> *auxNodePilha = top;
    top = top->next;
    delete auxNodePilha;
    tam--;
    return returnedItem;
}
};

#endif // PILHA2_H_INCLUDED
