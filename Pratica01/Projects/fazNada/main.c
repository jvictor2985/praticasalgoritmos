/*
 ============================================================================
 Name        : main.c
 Author      : Anderson Moreira
 E-mail		 : anderson.moreira@recife.ifpe.edu.br
 Version     : 11 de abril de 2011
 Copyright   :
 Description : Este programa � projetado para n�o fazer nada, e executar
  	  	  	  	 a fun��o sleep at� que o arquivo desapare�a, portanto,
  	  	  	  	 funciona bem como um programa que podemos ter
 	 	 	 	 milhares dele na mesma m�quina.
 	 	 	 	 gprof MuitosArquivos.exe gmon.out > analise.txt
 ============================================================================
 */


#include <stdio.h>

int main (int argc, char *argv[])
{
	int SleepSeconds;
    char Pathname[128];
    FILE *fd;

    if (argc < 3) {
        printf( "Este programa executa ate o arquivo (arg 2) desapareca. Fica em loop no sleep\n");
        printf( "Precisa de dois argumentos:\n");
        printf( "   1.  O numero em segundos para a funcao sleep\n");
        printf( "   2.  O caminho com nome do arquivo que deva existir para que o programa continue\n");
        return(0);
    }

    SleepSeconds = atoi (argv[1]);
    strcpy (Pathname, argv[2]);
    while ((fd = fopen (Pathname, "r")) != NULL)  {
        fclose (fd);
        sleep (SleepSeconds);
    }
}
