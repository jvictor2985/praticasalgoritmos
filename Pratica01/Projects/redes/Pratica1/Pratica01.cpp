/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   main.cpp
 * Author: ALUNO
 *
 * Created on 8 de Mar�o de 2018, 08:33
 */

#include <iostream>
#include "veiculo.h"
#include <chrono>

using namespace std;
#define AUX 10000


int main(int argc, char** argv) {
    cout << "Primeiraaplicacao C++" << endl;
	//Veiculo *v = new Veiculo("teste");
    //delete v;

    /*Veiculo veiculo1("v1");{
        Veiculo veiculo2("v2");{
            Veiculo veiculo3("v3");
        }
    }*/

    /*Veiculo *veiculo1 = new Veiculo("v1");{
        Veiculo *veiculo2= new Veiculo("v2");{
            Veiculo *veiculo3= new Veiculo("v3");
            veiculo3->setNumRodas(5);
            cout << "veiculo 3 tem " << veiculo3->getNumRodas() << " rodas." << endl;
            delete veiculo3;
            delete veiculo2;
            delete veiculo1;

        }
    }*/

    long timeInline = 0;
    long timeOutline = 0;
    Veiculo *vDesafio = new Veiculo("Desafio");

    auto outlineStart = std::chrono::high_resolution_clock::now();
    for(int i = 0; i < AUX; i++){
        vDesafio->setNumRodas(1);
    }
    auto outlineFinish = std::chrono::high_resolution_clock::now();
    timeOutline = std::chrono::duration_cast<std::chrono::nanoseconds>(outlineFinish-outlineStart).count() / 1000;

    auto inlineStart = std::chrono::high_resolution_clock::now();
    for(int i = 0; i < AUX; i++){
        vDesafio->setNumRodasIN(1);
    }
    auto inlineFinish = std::chrono::high_resolution_clock::now();
    timeInline = std::chrono::duration_cast<std::chrono::nanoseconds>(inlineFinish-inlineStart).count() / 1000;

    cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n======================================================================\n";

    cout << "Time outline(uS): " << timeOutline << endl;
    cout << "Time intline(uS): " << timeInline << endl;

    cout << "-------------------------------------------------" << endl;
    cout << "Diferenca: " << (timeOutline - timeInline) << endl;
    cout << "-------------------------------------------------" << endl;

    return 0;
}

