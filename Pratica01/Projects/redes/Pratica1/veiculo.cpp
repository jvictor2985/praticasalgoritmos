#include "veiculo.h"

int Veiculo::getNumRodas(){
    return this->num_rodas;
};
void Veiculo::setNumRodas(int numRodas){
    if(numRodas > 0){
        if(rodas > 0)
            delete rodas;
        rodas = new Roda[numRodas];
        this->num_rodas = numRodas;
    }
}
