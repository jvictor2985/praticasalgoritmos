#include <iostream>
#include "Veiculo.h"

using namespace std;

int main()
{
    /*Veiculo v1("v1");
    Terrestre v2("v2");
    Aquatico v3("v3");
    Aereo v4("v4");*/

    Veiculo * terr, * aqua, * aereo, * anfi;
    terr = new Terrestre("VT1");
    dynamic_cast<Terrestre *>(terr)->setCapacidadeMax(45);
    aqua= new Aquatico("VQ1");
    dynamic_cast<Aquatico *>(aqua)->setCargaMax(12.5);
    aereo = new Aereo("VA1");
    ((Aereo *)aereo)->setVelocidadeMax(1040.5);

    terr->mover();
    aqua->mover();
    aereo->mover();

    delete terr;
    delete aqua;
    delete aereo;

    anfi = new Anfibio("ANF1");
    anfi->mover();
    return 0;
}
