using namespace std;
#ifndef VEICULO_H_INCLUDED
#define VEICULO_H_INCLUDED
class Veiculo{

protected:
    string nome;

public:
    Veiculo(const char* nome){
        this->nome = nome;
        cout << "veiculo " << nome << " foi criado."<< endl;
    };
    virtual void mover() = 0;
    virtual ~Veiculo(){cout << "veiculo " << nome << " foi destruido."<< endl;}
};

class Terrestre: virtual public Veiculo{
private:
    int cap_pass; //N�mero m�ximo de passageiros
public:
    Terrestre(const char* nome): Veiculo(nome){
        this->nome = nome;
        this->cap_pass = 5;
        cout << "Terrestre " << nome << " foi criado."<< endl;
    };

    int getCapacidadeMax();
    void setCapacidadeMax(int newCapPass);

    virtual void mover();
    virtual ~Terrestre(){cout << "Terrestre " << nome << " foi destruido."<< endl;}
protected:
    Terrestre(): Veiculo("Construtor protegido de Terrestre"){
    }
};

class Aquatico: virtual public Veiculo{
private:
    float carga_max;//Carga m�xima em toneladas.
public:
    Aquatico(const char* nome): Veiculo(nome){
        this->nome = nome;
        this->carga_max = 10;
        cout << "Aquatico " << nome << " foi criado."<< endl;
    };

    float getCargaMax();
    void setCargaMax(int newCargaMax);

    virtual void mover();
    virtual ~Aquatico(){cout << "Aquatico " << nome << " foi destruido."<< endl;}
protected:
    Aquatico(): Veiculo("Construtor protegido de Aquatico"){
    }
};

class Aereo: public Veiculo{
private:
    float vel_max;//Velocidade m�xima em km/h.
public:
    Aereo(const char* nome): Veiculo(nome){
        this->nome = nome;
        this->vel_max = 100;
        cout << "Aereo " << nome << " foi criado."<< endl;
    };

    float getVelocidadeMax();
    void setVelocidadeMax(int newVelMax);

    virtual void mover();
    virtual ~Aereo(){cout << "Aereo " << nome << " foi destruido."<< endl;}
};

class Anfibio: public Terrestre, public Aquatico{
public:
    Anfibio (const char * nome): Veiculo(nome), Terrestre(), Aquatico(){}
    virtual void mover();
};


#include "Veiculo.cpp"
#endif // VEICULO_H_INCLUDED
