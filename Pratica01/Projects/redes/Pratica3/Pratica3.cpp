#include <iostream>
#include "funcoes.h"
#include "arranjo.h"
#include "aluno.h"

using namespace std;

int main(){
    //int x = 5, y = 10, z = 30;
    string x = "5.5", y = "10.15", z = "30.7";
    //trocar(x, y);
    /*cout << "Minimo entre " << x << " e " << y << " eh "
    << minimo(x, y) << endl;*/
    cout << "Maximo entre " << y << " e " << z << " eh "
    << maximo(y, z) << endl;

    Arranjo<int> arr(10);
    arr.set(4, 5);
    arr.set(7, 15);
    arr.set(8, 22);
    arr.exibir();

    Arranjo<float> arrFloat(5);
    arrFloat.set(0,2.3);
    arrFloat.set(1,4.5);
    arrFloat.set(2,8.8);
    arrFloat.set(3,9.1);
    arrFloat.exibir();

    Arranjo<Aluno> turma(3);
    turma.set(0, Aluno("Joao","1234"));
    turma.set(1, Aluno("Maria","5235"));
    turma.set(2, Aluno("Jose","2412"));
    turma.exibir();
    return 0;
}
