#ifndef PILHA_H_INCLUDED
#define PILHA_H_INCLUDED

using namespace std;
template <class T>
class Pilha {
private:
// propriedades para array de items, capacidade e topo da pilha
int capacidade;
int topoIdx;
T *items;
public:
Pilha(int capacidade) {
// instancia array de items, inicializa capacidade e topo
    this->capacidade = capacidade;
    this->topoIdx = 0;
    items = new T[this->capacidade];
}
~Pilha() {
// destroy array de items
delete[] items;
}
void empilha(T item) {
// empilha um item no topo da pilha; lan�a �Estouro da pilha� se cheia
    if(this->topoIdx + 1 < this->capacidade){
        this->topoIdx++;
        items[this->topoIdx] = item;
    }else{
        cout << "Estouro da pilha" << endl;
    }
}
T desempilha() {
// remove um item do topo da pilha; lan�a �Pilha vazia� se vazia
T currentItem;
if(this->topoIdx >= 0){
    currentItem = this->items[topoIdx];
    topoIdx--;
}else{
    cout << "Pilha vazia" << endl;
}
    return currentItem;
}
};


#endif // PILHA_H_INCLUDED
