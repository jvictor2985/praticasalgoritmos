#ifndef FILA2_H_INCLUDED
#define FILA2_H_INCLUDED
using namespace std;
template <class T>
struct nodeFila{
    nodeFila* next;
    T item;
};

template <class T>
class Fila {
private:
// array de itens, capacidade, tamanho, posi��o inicial, etc.
    nodeFila<T> *startQueue;
    nodeFila<T> *endQueue;
    int sizeQueue;
public:
Fila(int cap) {
// inicializar array de items, capacidade, tamanho, posi��o inicial
    sizeQueue = 0;
}
~Fila() {
// destruir array de itens
}
void enfileira(const T & item) {
// adiciona um item ao final da fila; lan�a �Fila cheia� caso cheia
    nodeFila<T> *newNodeFila = new nodeFila<T>;
    newNodeFila->item = item;
    if(vazia()){
        startQueue = newNodeFila;
    }else{
        //newNodeFila->next = endQueue;
        //endQueue = newNodeFila;
        //cout << "New Node Fila: " << newNodeFila << endl;
        //cout << "New Node Fila &: " << &newNodeFila << endl;
        endQueue->next = newNodeFila;
        //endQueue = newNodeFila;
    }
    endQueue = newNodeFila;
    sizeQueue++;
}
T desenfileira() {
// remove um item do inicio da fila; lan�a �Fila vazia� caso vazia
    nodeFila<T> *auxNodeFila = startQueue;
    T auxItem = startQueue->item;
    //cout << "Aux Node Fila: " << auxNodeFila << endl;
    //cout << "Aux Node Fila &: " << &auxNodeFila << endl;
    startQueue = startQueue->next;
    delete auxNodeFila;
    sizeQueue--;
    return auxItem;
}
int cheia() {
// retorna 1 se cheia, 0 caso contr�rio
    return 0;
}
int vazia() {
// retorna 1 se vazia, 0 caso contr�rio
    return sizeQueue == 0;
}
int tamanho() {
//retorna a quantidade de itens atualmente na fila
    return sizeQueue;
}
};


#endif // FILA2_H_INCLUDED
