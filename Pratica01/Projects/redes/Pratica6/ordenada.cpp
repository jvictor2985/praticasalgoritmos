#include <iostream>

using namespace std;

class ListaOrdenada {
private:
	int * items;
	int tamanho, capacidade;
public:
	ListaOrdenada(int cap) {
		this->capacidade = cap;
		this->tamanho = 0;
		items = new int[cap];
	}

	~ListaOrdenada() {
		delete [] items;
	}

	void insere(int key) {
	    cout<<"----------------------------------------------------------"<< key <<endl;
		if(tamanho < capacidade){
            for(int i = 0; i <= tamanho; i++){
                if(items[i] > key){
                    for(int j = tamanho; j >= i; j--){
                        cout << "Movendo " << items[j] << " em: " << j << " para: " << j + 1 <<endl;
                        items[j + 1] = items[j];
                    }
                    cout << "Inserindo " << key << " em: " << i <<endl;
                    items[i] = key;
                    break;
                }else if(i == tamanho){
                    items[tamanho] = key;
                }
            }
            tamanho++;
		}else{
		    cout << "lista cheia" << endl;
		}

		cout << "Tamanho: " << tamanho << "/" << capacidade << " Lista: ";
		for(int i = 0; i < tamanho; i++){
            cout << items[i] << " ";
		}
		cout << endl;
	}

	int buscaSequencial(int key) {
		for(int i = 0; i < tamanho; i++){
            if(key == items[i]){
                return i;
            }
		}
		return -1;
	}

	int buscaBinaria(int item) {
		return buscaBinaria(0, tamanho - 1, item);
	}

	int valida() {
		for (int i = 0; i < tamanho - 1; i++) {
			if (items[i] > items[i + 1]) return 0;
		}
		return 1;
	}

	void exibe() {
		for (int i = 0; i < tamanho; i++) {
			cout << i << ": " << items[i] << "; ";
		}
		cout << endl;
	}

private:

	int buscaBinaria(int inicio, int final, int item) {
		// implementar
	}

};


int main () {

	ListaOrdenada lista(10);

	int elementos [] = {10, 5, 25, 1, 5, 13, 50, 99, 33, 12};

	for (int i = 0; i < 10; i++) {
		lista.insere(elementos[i]);
	}

	cout << "Lista valida: " << (lista.valida()?"sim":"n�o") << endl;
	lista.exibe();

	int teste [] = {5, 7, 16, 99, 45, 12, 33, 1, 60, 6};

	for (int i = 0; i < 10; i++) {
		cout << "Buscando " << teste[i] << ": sequencial = " << lista.buscaSequencial(teste[i]) << " binaria = " << lista.buscaBinaria(teste[i]) << endl;

	}

}


