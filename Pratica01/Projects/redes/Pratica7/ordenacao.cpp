/*
 * ordenacao.cpp
 *
 *  Created on: 20 de out de 2017
 *      Author: ramide
 */

#include <iostream>
#include <stdlib.h>
#include <chrono>

using namespace std;

int validate(int * data, int size) {
	for (int i = 0; i < size - 1; i++) {
		if (data[i] > data[i + 1]) return 0;
	}
	return 1;
}

void print(int * data, int size) {
	for (int i = 0; i < size; i++) {
		cout << data[i] << " ";
	}
	cout << endl;
}

int * clone(int * array, int size) {
	int * copy = new int[size];
	for (int i = 0; i < size; i++) { copy[i] = array[i]; }
	return copy;
}

void swap(int &a, int &b) {
	int tmp = a;
	a = b;
	b = tmp;
}

void noopsort(int * array, int size) {
	// no-op
}


void bubblesort(int * array, int size) {
	//TO DO
	cout << "-----------BUBBLE----------" << endl;
	int last_unordered = size - 1;
	bool change = false;
	print(array, size);
	do{
    change = false;
        for(int i = 0; i < last_unordered; i++){
            if(array[i] > array[i + 1]){
                swap(array[i], array[i + 1]);
                print(array, size);
                change = true;
            }
        }
	}while(change);
}

void selectionsort(int * array, int size) {
	//TO DO
	cout << "-----------SELECTION----------" << endl;
	print(array, size);
	for(int i = 0; i < size; i++){
        int minId = i;
        for(int j = i + 1; j < size; j++){
            if(array[minId] > array[j])
                minId = j;
        }
        swap(array[i], array[minId]);
        print(array, size);
	}
}


void insertionsort(int * array, int size) {
	//TO DO
	cout << "-----------INSERTION----------" << endl;
	print(array, size);
	for(int i = 1; i < size; i++){
        int tmp = array[i];
        int j= i - 1;
        while(array[j] > tmp && j >= 0){
            array[j + 1] = array[j];
            j--;
        }
        array[j + 1] = tmp;
        print(array, size);
	}
}

void merge(int * target, int * buffer, int start, int mid, int finish) {
	//TO DO
	cout << "-----------MERGE----------" << endl;
	cout << "-----------START----------" << start << endl;
	cout << "-----------MID----------" << mid << endl;
	cout << "-----------FINISH----------" << finish << endl;
	int idx1 = start;
	int idx2;
    if(mid  == finish)
        idx2 = mid;
    else
        idx2 = mid + 1;
	for(int i = 0; i < finish; i++){
	    cout <<"A1[" << idx1 << "]: " <<buffer[idx1] <<" - A2[" << idx2 << "]: " <<buffer[idx2] <<endl;
        if(buffer[idx1] > buffer[idx2] && idx1 <= mid){
            cout <<"Win: " << idx1 <<endl;
            target[i] = buffer[idx1];
            idx1++;
        }else if(idx2 <= finish){
            cout <<"Win: " << idx2 <<endl;
            target[i] = buffer[idx2];
            idx2++;
        }else{
            cout <<"-BUG-"<<endl;
        }
	}
}

void mergesort(int * target, int * buffer, int start, int finish) {
	if (start >= finish) return;

	int mid = (start + finish)/2;

	mergesort(buffer, target, start, mid);
	mergesort(buffer, target, mid + 1, finish);

	merge(target, buffer, start, mid + 1, finish);
}

void mergesort(int * array, int size) {
	int * copy = clone(array, size);
	mergesort(array, copy, 0, size - 1);
	delete [] copy;
}

int partition(int * array, int start, int finish) {
	//TO DO
	return 0;
}

void quicksort(int * array, int start, int finish) {
	if (finish <= start) return;
	int pivot = partition(array, start, finish);
	quicksort(array, start, pivot - 1);
	quicksort(array, pivot + 1, finish);
}

void quicksort(int * array, int size) {
	quicksort(array, 0, size - 1);
}



void show(int * array, int size, const char * name, void function(int *, int), int printFlag) {
	int * copy = clone(array, size);
	auto start = std::chrono::high_resolution_clock::now();

	function(copy, size);

	auto finish = std::chrono::high_resolution_clock::now();
	long elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(finish-start).count() / 1000;

	int valid = validate(copy, size);
	cout << name << ": " << (valid?"ok":"erro") << " (tempo[us] = " << elapsed << ") ";

	if (printFlag) print(copy, size);
	delete [] copy;
}

int main() {
	int size = 10;
	int print = 1;

	int * array = new int[size];

	for (int i = 0; i < size; i++) {
		array[i] = rand() % size;
	}

	show(array, size, "NoOpSort     ", noopsort, print);
	show(array, size, "BubbleSort   ", bubblesort, print);
	show(array, size, "SelectionSort", selectionsort, print);
	show(array, size, "InsertionSort", insertionsort, print);
	show(array, size, "MergeSort    ", mergesort, print);
	//show(array, size, "QuickSort    ", quicksort, print);

	delete [] array;
}






