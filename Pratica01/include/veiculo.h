#ifndef VEICULO_H
#define VEICULO_H
using namespace std;
#include <iostream>

class Roda{
public:
    Roda(){
        cout <<"Objeto roda criado com sucesso"<<endl<<endl;
    };
    ~Roda(){
        cout<<"Objeto roda destruido com sucesso"<<endl<<endl;
    }
};

class Veiculo{
private:
     string nome;
     int num_rodas;
     Roda * rodas;

public:
    Veiculo(const char * name){
        this->nome = name;
        this->rodas = NULL;
        cout << "Objeto " <<nome<< " criado com sucesso" <<endl<<endl;
    }
    ~Veiculo(){
        cout << "Objeto " <<nome<< " destruido com sucesso"<<endl<<endl;
        delete[] rodas;
    }
        void setNumRodas(int valor);
        int getNumRodas();
};
#endif // VEICULO_H
