#include <iostream>
#include "Veiculo.h"

using namespace std;

int main(){
cout << "Primeira Aplicacao C++" <<endl<<endl;

//Cria��o de um novo objeto chamado v1
Veiculo * obj1 = new Veiculo("v1");
{
    //Cria��o de um novo objeto chamado v2
    Veiculo * obj2 = new Veiculo("v2");
    {
        //Cria��o de um novo objeto chamado v3
        Veiculo * obj3 = new Veiculo("v3");
        {
            //Destrui��o do objeto v3
            delete(obj3);
        }
        //Destrui��o do objeto v3
        delete(obj2);
    }
    //Destrui��o do objeto v3
    delete(obj1);
}
//Cria��o de um novo objeto chamado v1
Veiculo * obj4 = new Veiculo("v4");
//Cria��o de quatro objetos do tipo roda
obj4->setNumRodas(4);
//Cria��o de cinco objetos do tipo roda
obj4->setNumRodas(5);
cout<<"Numero de rodas:"<<obj4->getNumRodas()<<endl<<endl;
delete obj4;
return 0;
}
