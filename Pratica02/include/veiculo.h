#include <iostream>
#ifndef VEICULO_H
#define VEICULO_H
using namespace std;

class Veiculo
{
    public:
        Veiculo(const char * name){
        this->nome = name;
        cout << "Construtor ve�culo: Objeto "<<nome<<" criado com sucesso"<<endl<<endl;
        };
        virtual ~Veiculo(){
        cout << "Destrutor ve�culo: Objeto "<<nome<<" destru�do com sucesso" <<endl<<endl;
        };
        virtual void mover() = 0;
    protected:
        string nome;
};

class Terrestre : public virtual Veiculo{
public:
    	Terrestre(const char * name):Veiculo(name){
    	this->nome=name;
    	cout<<"Construtor Terrestre: Objeto "<<nome<<" criado com sucesso"<<endl<<endl;
    	};
    	virtual ~Terrestre(){
    	cout<<"Destrutor terrestre: Objeto "<<nome<<" destruido com sucesso"<<endl<<endl;
    	};
    	void setCapacidadeMax(int valor);
        int getCapacidadeMax();
        virtual void mover();
private:
    	string nome;
    	int cap_pass = 5;
protected:
       Terrestre():Veiculo("qualquer"){
        cout<<"Construtor Terrestre: Objeto "<<nome<<" criado com sucesso"<<endl<<endl;
       };
};

class Aquatico : public virtual Veiculo{
public:
    	Aquatico(const char * name):Veiculo(name){
        this->nome=name;
    	cout<<"Construtor Aqu�tico: Objeto veiculo aquatico criado com sucesso"<<endl<<endl;
    	};
    	virtual ~Aquatico(){
    	cout<<"Destrutor Aqu�tico: Objeto veiculo aquatico destruido com sucesso"<<endl<<endl;
    	};
    	void setCargaMax(float valor);
    	float getCargaMax();
        virtual void mover();
private:
        string nome;
        float carga_max = 10;
protected:
        Aquatico(): Veiculo("qualquer"){
            cout<<"Construtor Aqu�tico: Objeto veiculo aquatico criado com sucesso"<<endl<<endl;
        };
};

class Aereo : public Veiculo{
public:
    	Aereo(const char * name):Veiculo(name){
        this->nome=name;
    	cout<<"Destrutor �ereo: Objeto veiculo aereo criado com sucesso"<<endl<<endl;
    	};
    	virtual ~Aereo(){
    	cout<<"Destrutor �ereo: Objeto veiculo aereo destruido com sucesso"<<endl<<endl;
    	};
    	void setVelocidadeMax(float valor);
    	float getVelocidadeMax();
        virtual void mover();
private:
        string nome;
        float vel_max = 100;
};

class Anfibio : public Terrestre, public Aquatico{
public:
        Anfibio(const char * nome) : Veiculo(nome), Terrestre(), Aquatico(){
            this->name = nome;
        };
        virtual ~Anfibio(){
        cout<<"Destrutor Anf�bio: Objeto ve�culo anf�bio destru�do com sucesso"<<endl<<endl;
        };
        virtual void mover();

private:
        string name;
};
#endif // VEICULO_H
