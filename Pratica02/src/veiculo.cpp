#include <iostream>
#include "veiculo.h"

using namespace std;
void Terrestre::setCapacidadeMax(int valor){
    this->cap_pass=valor;
}

int Terrestre::getCapacidadeMax(){
    return cap_pass;
}

void Terrestre::mover(){
    cout<<"Movimenta��o terrestre: Ve�culo terreste "<<nome<<" moveu-se"<<endl<<endl;
}

void Aquatico::setCargaMax(float valor){
    this->carga_max=valor;
}

float Aquatico::getCargaMax(){
    return carga_max;
}

void Aquatico::mover(){
    cout<<"Movimenta��o terrestre: Ve�culo aquatico "<<nome<<" moveu-se"<<endl<<endl;
}

void Aereo::setVelocidadeMax(float valor){
    this->vel_max=valor;
}

float Aereo::getVelocidadeMax(){
    return vel_max;
}

void Aereo::mover(){
    cout<<"Movimenta��o terrestre: Ve�culo aereo "<<nome<<" moveu-se"<<endl<<endl;
}

void Anfibio::mover(){
    //cout<<"Movimenta��o anf�bia: Ve�culo anf�bio "<<this->name<<" moveu-se"<<endl<<endl;
    Terrestre::mover();
    Aquatico::mover();
}
