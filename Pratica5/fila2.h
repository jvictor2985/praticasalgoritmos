#ifndef FILA2_H_INCLUDED
#define FILA2_H_INCLUDED
using namespace std;
template <class T>
struct nodeFila {
    nodeFila* next;
    T item;
};

template <class T>
class Fila {
private:
// array de itens, capacidade, tamanho, posi��o inicial, etc.
    nodeFila<T> *inicioFila;
    nodeFila<T> *fimFila;
    int tamFila;
public:
    Fila(int cap) {
// inicializar array de items, capacidade, tamanho, posi��o inicial
        tamFila = 0;
    }
    ~Fila() {
// destruir array de itens
    for(int i = 0; i < tamFila; i++){
        desenfileira();
    }
    }
    void enfileira(const T & item) {
// adiciona um item ao final da fila; lan�a �Fila cheia� caso cheia
        nodeFila<T> *newNodeFila = new nodeFila<T>;
        try {
            newNodeFila->item = item;
            if(vazia()) {
                inicioFila = newNodeFila;
            } else {
                fimFila->next = newNodeFila;
            }
            fimFila = newNodeFila;
            tamFila++;
        } catch(T e) {
            cout<<"Erro: "<<e<<endl<<endl;
        }

    }
    T desenfileira() {
// remove um item do inicio da fila; lan�a �Fila vazia� caso vazia
        nodeFila<T> *auxNodeFila = inicioFila;
        try {
            T auxItem = inicioFila->item;
            inicioFila = inicioFila->next;
            delete auxNodeFila;
            tamFila--;
            if(tamFila == 0){
                fimFila = NULL;
            }
            return auxItem;
        } catch(T e) {
            cout<<"Erro: "<<e<<endl<<endl;
        }
    }
    int cheia() {
// retorna 1 se cheia, 0 caso contr�rio
        return 0;
    }
    int vazia() {
// retorna 1 se vazia, 0 caso contr�rio
        return tamFila == 0;
    }
    int tamanho() {
//retorna a quantidade de itens atualmente na fila
        return tamFila;
    }
};


#endif // FILA2_H_INCLUDED
