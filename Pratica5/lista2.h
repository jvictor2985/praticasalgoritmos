#ifndef LISTA2_H_INCLUDED
#define LISTA2_H_INCLUDED

using namespace std;

template <class T>
struct nodeLista {
    nodeLista *next;
    T item;
};

template <class T>
class Lista {
private:
// itens da lista, capacidade e tamanho atual
    nodeLista<T> *startNode;
    nodeLista<T> *endNode;
    int listSize;

public:
    Lista(int capacidade) {
// iniciliza��o do array, capacidade e tamanho
        listSize = 0;
    }
    ~Lista() {
//destrui��o do array
        for(int i = 0; i < listSize; i++) {
            remove(1);
        }
    }
    void adiciona (const T & item) {
// adiciona um item ao final da lista; lan�a �Lista cheia� caso cheia
        try {
            nodeLista<T> *newNode = new nodeLista<T>;
            newNode->item = item;
            if(listSize == 0) {
                startNode = newNode;
                endNode = newNode;
            } else {
                endNode->next = newNode;
                endNode = newNode;
            }
        } catch(T e) {
            throw e;
        }

        listSize++;
    }
    T pega(int idx) {
// pega um item pelo indice (come�a em 1);
// lan�a �Item inv�lido� se posi��o inv�lida
        try {
            nodeLista<T> *auxNode = startNode;
            for(int i = 1; i < idx; i++) {
                auxNode = auxNode->next;
            }
            return auxNode->item;
        } catch(T e) {
            throw e;
        }
    }
    void insere (int idx, const T & item) {
// insere um item na posi��o indicada
// lan�a �Lista cheia� caso cheia
// lan�a �Item inv�lido� se posi��o inv�lida
// desloca itens existentes para a direita
        try {
            nodeLista<T> *nodeAnterior = startNode;
            if(idx == 1) {
                nodeAnterior->next = startNode;
                startNode = nodeAnterior;
            } else if(idx >= 1 && idx < listSize) {
                for(int i = 1; i < idx - 1; i++) {
                    nodeAnterior = nodeAnterior->next;
                }
                nodeLista<T> *auxNode = new nodeLista<T>;
                auxNode->item = item;
                auxNode->next = nodeAnterior->next;
                nodeAnterior->next = auxNode;
                listSize++;
            }
        } catch(T e) {
            throw e;
        }

    }
    void remove(int idx) {
// remove item de uma posi��o indicada
// lan�a �Item inv�lido� se posi��o inv�lida
// desloca itens para a esquerda sobre o item removido
        try {
            nodeLista<T> *nodeAnterior = startNode;
            if(idx == 1) {
                nodeAnterior = startNode;
                startNode = startNode->next;
                if(endNode == nodeAnterior) {
                    endNode = NULL;
                }
                listSize--;
            } else if(idx > 1 && idx <= listSize) {
                for(int i = 1; i < idx - 1; i++) {
                    nodeAnterior = nodeAnterior->next;
                }
                nodeLista<T> *auxNode = nodeAnterior->next;
                nodeAnterior->next = auxNode->next;
                if(endNode == auxNode) {
                    endNode = nodeAnterior;
                }
                delete auxNode;
                listSize--;
            }
        } catch(T e) {
            throw e;
        }

    }
    void exibe() {
// exibe os itens da saida padr�o separados por espa�os
        nodeLista<T> *auxNode = startNode;
        for(int i = 0; i < listSize; i++) {
            cout << auxNode->item << " ";
            auxNode = auxNode->next;
        }
        cout << endl;
    }
};

#endif // LISTA2_H_INCLUDED

