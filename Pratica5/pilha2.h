#ifndef PILHA2_H_INCLUDED
#define PILHA2_H_INCLUDED
#include <iostream>
using namespace std;

template <class T>
struct nodePilha {
    nodePilha *prox;
    T item;
};

template <class T>
class Pilha {
private:
    nodePilha<T> *topo;
    int tam_maximo=0;
public:
    Pilha(int capacidade) {
        this->tam_maximo = capacidade;
    }
    ~Pilha() {
        for(int i = 0; i < tam_maximo; i++){
        desempilha();
    }
    }

    void empilha(T item) {
        nodePilha<T> *newNodePilha = new nodePilha<T>;
        try {
            newNodePilha->prox = topo;
            newNodePilha->item = item;
            topo = newNodePilha;
            tam_maximo++;
        } catch(T e) {
            cout<<"Erro: "<<e<<endl<<endl;
        }

    }
    T desempilha() {
        try {
            T retunedItem = topo->item;
            nodePilha<T> *auxNodePilha = topo;
            topo = topo->prox;
            delete auxNodePilha;
            tam_maximo--;
            return retunedItem;
        } catch(T e) {
            cout<<"Erro: "<<e<<endl<<endl;
        }

    }
};
#endif // PILHA2_H_INCLUDED
