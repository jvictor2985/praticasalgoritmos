/*
 * graph.cpp

 *
 *  Created on: 10 de nov de 2017
 *      Author: ramide
 */
#include "graph.h"
#include <iostream>
using namespace std;
Graph::Graph(int numVertices) {
	this->numVertices = numVertices;

	used = new int[numVertices];
	group = new int[numVertices];
	matrix = new int*[numVertices];

	for (int i = 0; i < numVertices; i++) {
		used[i] = 0;
		group[i] = -1;
		matrix[i] = new int[numVertices];
		for (int n = 0; n < numVertices; n++)
			matrix[i][n] = 0;
	}
}

Graph::~Graph() {
	for (int i = 0; i < numVertices; i++)
		delete [] matrix[i];

	delete [] matrix;
	delete [] used;
	delete [] group;
}


void Graph::edge(int v1, int v2) {
	// TODO
	matrix[v1][v2]++;
	matrix[v2][v1]++;
}


// Indica se existe caminho entre v1 e v2,
// isto �, se eles fazem parte do mesmo grupo
int Graph::_find(int v1, int v2) {
	return (group[v1] == group[v2]);
}
// Fa�a a uni�o dos grupos de v1 e v2
// Ao final, group[v1] == group[v2],
// e todos os n�s que faziam parte do grupo de v1
// agora fazem o parte do grupo de v2 (e vice-versa)
void Graph::_union(int v1, int v2) {
	if (group[v1] == group[v2]) return;

	for (int k = 0; k < numVertices; k++) {
		if (group[k] == group[v2])
			group[k] = group[v1];
	}
}

// Ver descri��o na pr�tica
int Graph::connected() {
	// TODO

	// Inicializar grupos
    for(int i=0;i < numVertices; i++){
        for(int j = 0; j < numVertices; j++){
            if(matrix[i][j] != 0){
                group[i] = i;
                used[i] = 1;
            }
        }
    }

	// Para todos as arestas, fa�a a uni�o dos dois v�rtices
	// (isto �, agora eles s�o parte do mesmo grupo)
     for(int i = 0; i < numVertices; i++){
        for(int j = 0; j < numVertices; j++){
            if(matrix[i][j] != 0){
                _union(i,j);
            }
        }
     }
	// Para todos os n�s (usados), verifique se est�o no mesmo grupo
	// (isto �, grafo o grafo � conexo)
    int allGroupPos = -1;
     for(int i = 0; i< numVertices; i++){
        if(used[i]){
            if(allGroupPos == -1){
                allGroupPos = i;
            }else if(!_find(i, allGroupPos)){
                return 0;
            }
        }
     }
	return 1;
}

int Graph::degree(int v) {
	// TODO
	int myDegree = 0;
	for(int i = 0;i < numVertices; i++){
        myDegree += matrix[v][i];
	}
	return myDegree;
}



